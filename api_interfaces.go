package binance

import "gitlab.com/cluttered-code/binance-us-go/model"

// PublicClient contains public API and WebSocket functions
type PublicClient interface {
	GeneralAPI
	PublicStreamer
}

// SecureClient is the secure API and WebSocket functions
type SecureClient interface {
	PublicClient
	UserStreamAPI
	SecureStreamer
}

// GeneralAPI contains the general API functions
type GeneralAPI interface {
	Ping() error
	Time() (*model.Time, error)
}

// UserStreamAPI is the user stream API functions
type UserStreamAPI interface {
	UserStreamStart() (*model.UserStream, error)
	UserStreamKeepAlive(listenKey string) error
	UserStreamClose(listenKey string) error
}
