package binance

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/cluttered-code/binance-us-go/model"
)

func klineParser(input []byte) interface{} {
	var event *model.KlineEvent
	_ = json.Unmarshal(input, &event)
	return &event.Kline
}

func (c *client) Klines(symbol string, interval Interval) <-chan *model.Kline {
	path := fmt.Sprintf("/ws/%s@kline_%s", strings.ToLower(symbol), interval)
	groupFn, wsChan := c.callWS(path, klineParser)

	klineChan := make(chan *model.Kline)
	go func() {
		for {
			i := <-wsChan
			if i != nil {
				klineChan <- i.(*model.Kline)
			}
		}
	}()

	c.group.Go(groupFn)

	return klineChan
}
