package binance

import "fmt"

// APIError represents an API error response
type APIError struct {
	Code    int64  `json:"code"`
	Message string `json:"msg"`
}

func (e *APIError) Error() string {
	return fmt.Sprintf("APIError: code=%d message='%s'", e.Code, e.Message)
}
