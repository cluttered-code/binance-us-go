package main

import (
	"context"
	"time"

	"gitlab.com/cluttered-code/binance-us-go"
	"gitlab.com/cluttered-code/binance-us-go/examples/helper"
	"go.uber.org/zap"

	"golang.org/x/sync/errgroup"
)

func klineHandler(gctx context.Context, client binance.PublicClient, symbol string, interval binance.Interval) func() error {
	return func() error {
		klines := client.Klines(symbol, interval)
		go func() {
			for {
				kline := <-klines
				zap.S().Infof("%+v", kline)
			}
		}()

		// wait for context to close
		select {
		case <-gctx.Done():
			zap.S().Infof("close kline handler %s::%s", symbol, interval)
			return gctx.Err()
		}
	}
}

func main() {
	ctx, done := context.WithCancel(context.Background())
	group, gctx := errgroup.WithContext(ctx)
	binanceClient := binance.NewPublicClient(binance.WithContext(gctx))

	group.Go(helper.SignalHandler(gctx, done))
	group.Go(klineHandler(gctx, binanceClient, "BTCUSD", binance.ONE_MINUTE))
	group.Go(klineHandler(gctx, binanceClient, "ADAUSD", binance.THREE_MINUTES))

	// wait for termination (fail when not canceled by signal)
	if err := group.Wait(); err != nil && err != context.Canceled {
		zap.S().Fatal(err)
	}

	time.Sleep(1 * time.Second) // allow shutdown logs
	zap.S().Info("done")
	zap.S().Sync()
}
