package helper

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"
)

// SignalHandler handles SIGINT and SIGTERM
func SignalHandler(gctx context.Context, done context.CancelFunc) func() error {
	return func() error {
		signals := make(chan os.Signal)
		signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

		select {
		case sig := <-signals:
			zap.S().Infof("signal: %s", sig)
			done()
		case <-gctx.Done():
			zap.S().Info("close signal handler")
			return gctx.Err()
		}

		return nil
	}
}
