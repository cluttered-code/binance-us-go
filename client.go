package binance

import (
	"context"
	"golang.org/x/sync/errgroup"
)

type client struct {
	key    string
	secret string
	ctx    context.Context
	done   context.CancelFunc
	gctx   context.Context
	group  *errgroup.Group
}

// verify Client methods are implemented
var _ SecureClient = (*client)(nil)

func (c *client) Close() {
	c.done()
}

type ClientOption func(c *client)

// WithContext sets the parent context of the client
func WithContext(ctx context.Context) ClientOption {
	return func(c *client) {
		c.ctx = ctx
	}
}

// NewPublicClient creates a public instance of the client
func NewPublicClient(opts ...ClientOption) PublicClient {
	return NewClient("", "", opts...)
}

// NewClient creates a secure instance of the Client
func NewClient(key string, secret string, opts ...ClientOption) SecureClient {
	c := &client{
		key:    key,
		secret: secret,
		ctx:    context.Background(),
	}

	for _, opt := range opts {
		opt(c)
	}

	c.ctx, c.done = context.WithCancel(c.ctx)
	c.group, c.gctx = errgroup.WithContext(c.ctx)

	return c
}
