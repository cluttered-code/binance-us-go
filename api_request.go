package binance

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

// Headers
const (
	headerAPIKey = "X-MBX-APIKEY"
)

type requestOption func(*client, *http.Request)

func withKeyHeader() requestOption {
	return func(c *client, req *http.Request) {
		req.Header.Set(headerAPIKey, c.key)
	}
}

func withParam(key string, value string) requestOption {
	return func(c *client, req *http.Request) {
		q := req.URL.Query()
		q.Set(key, value)
		req.URL.RawQuery = q.Encode()
	}
}

func (c *client) get(path string, resModel interface{}, opts ...requestOption) error {
	err := c.callAPI(http.MethodGet, path, resModel, opts...)
	return err
}

func (c *client) post(path string, resModel interface{}, opts ...requestOption) error {
	err := c.callAPI(http.MethodPost, path, resModel, opts...)
	return err
}

func (c *client) put(path string, resModel interface{}, opts ...requestOption) error {
	err := c.callAPI(http.MethodPut, path, resModel, opts...)
	return err
}

func (c *client) delete(path string, resModel interface{}, opts ...requestOption) error {
	err := c.callAPI(http.MethodDelete, path, resModel, opts...)
	return err
}

func (c *client) callAPI(method string, path string, resModel interface{}, opts ...requestOption) error {
	req := buildRequest(method, path)

	// add options
	for _, opt := range opts {
		opt(c, req)
	}

	// perform request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	err = unmarshal(res, resModel)
	if err != nil {
		return err
	}

	return nil
}

func buildRequest(method string, path string) *http.Request {
	uri := url.URL{Scheme: apiScheme, Host: apiHost, Path: path}
	req, _ := http.NewRequest(method, uri.String(), nil)
	return req
}

func unmarshal(res *http.Response, resModel interface{}) error {
	// read response body
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	// unmarshal errors
	if res.StatusCode >= 400 {
		var apiError *APIError
		err := json.Unmarshal(resBody, &apiError)
		if err != nil {
			return err
		}
		return apiError
	}

	// unmarshal response
	if resModel != nil {
		err = json.Unmarshal(resBody, resModel)
		if err != nil {
			return err
		}
	}

	return nil
}
