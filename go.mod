module gitlab.com/cluttered-code/binance-us-go

go 1.15

require (
	github.com/gorilla/websocket v1.4.2
	go.uber.org/zap v1.16.0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
)
