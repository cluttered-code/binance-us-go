package model

// Time represents a server time response
type Time struct {
	ServerTime uint64 `json:"serverTime"`
}
