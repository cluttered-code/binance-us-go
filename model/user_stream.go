package model

// UserStream represents a user stream response
type UserStream struct {
	ListenKey string `json:"listenKey"`
}
