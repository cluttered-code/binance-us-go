package model

// KlineEvent represents a KlineEvent
type KlineEvent struct {
	Type   string `json:"e"`
	Time   int64  `json:"E"`
	Symbol string `json:"s"`
	Kline  Kline  `json:"k"`
}

// Kline represents a Kline
type Kline struct {
	Symbol         string `json:"s"`
	Interval       string `json:"i"`
	Start          int64  `json:"t"`
	Stop           int64  `json:"T"`
	FirstID        int64  `json:"f"`
	LastID         int64  `json:"L"`
	Open           string `json:"o"`
	High           string `json:"h"`
	Low            string `json:"l"`
	Close          string `json:"c"`
	Trades         int64  `json:"n"`
	Closed         bool   `json:"x"`
	BaseVolume     string `json:"v"`
	BuyBaseVolume  string `json:"V"`
	QuoteVolume    string `json:"q"`
	BuyQuoteVolume string `json:"Q"`
}
