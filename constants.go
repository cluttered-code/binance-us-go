package binance

// API URI
const (
	apiScheme = "https"
	apiHost   = "api.binance.us"
)

// WebSocket URI
const (
	wsScheme = "wss"
	wsHost   = "stream.binance.us:9443"
)

// Interval replesents an interval
type Interval string

// canstant intervals
const (
	ONE_MINUTE      Interval = "1m"
	THREE_MINUTES   Interval = "3m"
	FIVE_MINUTES    Interval = "5m"
	FIFTEEN_MINUTES Interval = "15m"
	THIRTY_MINUTES  Interval = "30m"
	ONE_HOUR        Interval = "1h"
	TWO_HOURS       Interval = "2h"
	FOUR_HOURS      Interval = "4h"
	SIX_HOURS       Interval = "6h"
	EIGHT_HOURS     Interval = "8h"
	TWELVE_HOURS    Interval = "12h"
	ONE_DAY         Interval = "1d"
	THREE_DAYS      Interval = "3d"
	ONE_WEEK        Interval = "1w"
	ONE_MONTH       Interval = "1M"
)

func (i Interval) IsValid() bool {
	switch i {
	case ONE_MINUTE, THREE_MINUTES, FIVE_MINUTES, FIFTEEN_MINUTES, THIRTY_MINUTES,
		ONE_HOUR, TWO_HOURS, FOUR_HOURS, SIX_HOURS, EIGHT_HOURS, TWELVE_HOURS,
		ONE_DAY, THREE_DAYS, ONE_WEEK, ONE_MONTH:
		return true
	default:
		return false
	}
}
