package binance

import (
	"gitlab.com/cluttered-code/binance-us-go/model"
)

const (
	pathUserStream = "/api/v3/userDataStream"
	paramListenKey = "listenKey"
)

func (c *client) UserStreamStart() (*model.UserStream, error) {
	var userStream *model.UserStream
	err := c.post(pathUserStream, &userStream,
		withKeyHeader())
	return userStream, err
}

func (c *client) UserStreamKeepAlive(listenKey string) error {
	err := c.put(pathUserStream, nil,
		withKeyHeader(),
		withParam(paramListenKey, listenKey),
	)
	return err
}

func (c *client) UserStreamClose(listenKey string) error {
	err := c.delete(pathUserStream, nil,
		withKeyHeader(),
		withParam(paramListenKey, listenKey),
	)
	return err
}
