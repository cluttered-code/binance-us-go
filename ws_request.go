package binance

import (
	"net/url"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type wsMessageParser func([]byte) interface{}

func (c *client) callWS(path string, parse wsMessageParser) (func() error, <-chan interface{}) {
	outputs := make(chan interface{})
	fn := func() error {
		uri := &url.URL{Scheme: wsScheme, Host: wsHost, Path: path}
		uriStr := uri.String()
		zap.S().Infof("connecting to %s", uriStr)
		conn, _, err := websocket.DefaultDialer.Dial(uriStr, nil)
		if err != nil {
			return err
		}

		// read from websocket
		errs := make(chan error)
		go func() {
			for {
				_, message, err := conn.ReadMessage()
				if err != nil {
					errs <- err
					close(errs)
					return
				} else if message != nil {
					outputs <- parse(message)
				}
			}
		}()

		select {
		case err = <-errs:
		case <-c.gctx.Done():
			err = c.gctx.Err()
		}

		close(outputs)
		conn.Close()
		zap.S().Infof("disconnected from %s", uriStr)

		return err
	}

	return fn, outputs
}
