package binance

import "gitlab.com/cluttered-code/binance-us-go/model"

// PublicStreamer contains all the public WebSocket functions
type PublicStreamer interface {
	KlineStreamer
	Close()
}

// SecureStreamer contains all the public and secure WebSocket functions
type SecureStreamer interface {
	PublicStreamer
}

// KlineStreamer streams klines (candlesticks)
type KlineStreamer interface {
	Klines(string, Interval) <-chan *model.Kline
}
