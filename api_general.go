package binance

import "gitlab.com/cluttered-code/binance-us-go/model"

// Ping the API server to test connection
func (c *client) Ping() error {
	path := "/api/v3/ping"
	err := c.get(path, nil)
	return err
}

// Time returns the current server UTC time
func (c *client) Time() (*model.Time, error) {
	path := "/api/v3/time"
	var serverTime *model.Time
	err := c.get(path, &serverTime)
	return serverTime, err
}
